﻿using UnityEngine;

public class Game : PerfectStateMachine
{
    void Start()
    {
        ChangeState<InitialAnimationState>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            ToggleMenu();
    }

    public void ToggleMenu()
    {
        if (CurrentState.Is<PlayState>())
            ChangeState<MenuState>();
        else if (CurrentState.Is<MenuState>())
            ChangeState<PlayState>();
    }
}

﻿using UnityEngine;

public class Cube : MonoBehaviour
{
    public Vector3 PatrolPoint1;
    public Vector3 PatrolPoint2;
    [Range(1,10)]
    public float PatrolTime;

    private Vector3 _patrolStartPosition;
    private Vector3 _patrolCurrentTarget;
    private Vector3 _patrolNextTarget;
    private float _patrolTimeElapsed;

    void Awake()
    {
        _patrolCurrentTarget = PatrolPoint1;
        _patrolNextTarget = PatrolPoint2;
        _patrolStartPosition = transform.position;
        enabled = false;
    }

    void Update()
    {
        _patrolTimeElapsed += Time.deltaTime;
        if (_patrolTimeElapsed < PatrolTime)
        {
            var newPos = Vector3.Lerp(_patrolStartPosition, _patrolCurrentTarget, _patrolTimeElapsed / PatrolTime);
            transform.position = newPos;
        }
        else
        {
            transform.position = _patrolCurrentTarget;
            var currentTarget = _patrolCurrentTarget;
            _patrolCurrentTarget = _patrolNextTarget;
            _patrolNextTarget = currentTarget;
            _patrolStartPosition = transform.position;
            _patrolTimeElapsed = 0;
        }
    }

    public void Patrol()
    {
        enabled = true;
    }

    public void Stop()
    {
        enabled = false;
    }
}

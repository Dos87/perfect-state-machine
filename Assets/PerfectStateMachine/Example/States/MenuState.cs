﻿using UnityEngine;

public class MenuState : PerfectState
{
    private GameObject _menu;

    void Awake()
    {
        _menu = GameObject.Find("Menu");
        _menu.SetActive(false);
    }

    protected override void OnEnter()
    {
        _menu.SetActive(true);
    }

    protected override void OnExit()
    {
        _menu.SetActive(false);
    }
}

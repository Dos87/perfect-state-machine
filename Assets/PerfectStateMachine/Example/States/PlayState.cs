﻿using UnityEngine;

public class PlayState : PerfectState
{
    private Cube _cube;
    private GameObject _ui;

    void Awake()
    {
        _cube = FindObjectOfType<Cube>();
        _ui = GameObject.Find("UI");
        _ui.SetActive(false);
    }

    protected override void OnEnter()
    {
        _cube.Patrol();
        _ui.SetActive(true);
    }

    protected override void OnExit()
    {
        _cube.Stop();
        _ui.SetActive(false);
    }
}

﻿using UnityEngine;

public class InitialAnimationState : PerfectState
{
    public Camera AnimationCamera;
    public Vector3 CameraTargetPosition;
    [Range(1, 5)]
    public int AnimationTime;

    private Vector3 _animationCameraStartPosition;
    private float _animationTimeElapsed;

    void Awake()
    {
        if (AnimationCamera == null)
            Debug.LogError("InitialAnimationState should have an AnimationCamera");
    }

    protected override void OnEnter()
    {
        _animationCameraStartPosition = AnimationCamera.transform.position;
        _animationTimeElapsed = 0;
        enabled = true;
    }

    protected override void OnExit()
    {
        enabled = false;
    }

    void Update()
    {
        _animationTimeElapsed += Time.deltaTime;
        if (_animationTimeElapsed < AnimationTime)
        {
            var newPos = Vector3.Lerp(_animationCameraStartPosition, CameraTargetPosition, _animationTimeElapsed / AnimationTime);
            AnimationCamera.transform.position = newPos;
        }
        else
        {
            AnimationCamera.transform.position = CameraTargetPosition;
            GoToTheNextState();
        }
    }
}

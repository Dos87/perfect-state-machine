﻿using UnityEngine;

public abstract class PerfectState : MonoBehaviour
{
    public PerfectState NextState;
    public PerfectState PreviousState { get; private set; }

    private PerfectStateMachine _stateMachine;
    private bool _isActive;

    public void Enter(PerfectStateMachine stateMachine)
    {
        if (_isActive)
        {
            Debug.LogErrorFormat("Can not be Enter into '{0}', because it is already active", this);
            return;
        }

        _isActive = true;
        _stateMachine = stateMachine;
        PreviousState = _stateMachine.PreviousState;
        Debug.LogFormat("Enter to '{0}'", this);
        OnEnter();
    }

    public void Exit()
    {
        Debug.LogFormat("Exit from '{0}'", this);
        OnExit();
        _isActive = false;
        _stateMachine = null;
    }

    protected virtual void OnEnter()
    {
    }

    protected virtual void OnExit()
    {
    }

    protected void GoToTheNextState()
    {
        if (NextState != null)
            _stateMachine.ChangeState(NextState);
        else
            Debug.LogWarningFormat("'{0}' can not go to the next state, because it has no next state", this);
    }

    protected void GoToTheState<TState>() where TState : PerfectState
    {
        _stateMachine.ChangeState<TState>();
    }

    public void SetNextState<TState>() where TState : PerfectState
    {
        NextState = _stateMachine.GetState<TState>();
    }

    public bool Is<TState>() where TState : PerfectState
    {
        return this is TState;
    }

    public bool Is(PerfectState otherState)
    {
        return GetType() == otherState.GetType();
    }

    public override string ToString()
    {
        return GetType().Name;
    }
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PerfectStateMachine : MonoBehaviour
{
    public PerfectState CurrentState { get; private set; }
    public PerfectState PreviousState { get; private set; }

    private bool _isInited;
    private Dictionary<Type, PerfectState> _states;

    private void Init()
    {
        _isInited = true;

        _states = new Dictionary<Type, PerfectState>();
        foreach (var state in GetComponentsInChildren<PerfectState>())
            _states.Add(state.GetType(), state);
    }

    public void ChangeState<TState>() where TState : PerfectState
    {
        PerfectState newState;
        if (TryGetState<TState>(out newState))
            ChangeState(newState);
        else
            Debug.LogErrorFormat("Unable to change the State on the '{0}'! '{0}' should be child of '{1}'", typeof(TState).Name, typeof(PerfectStateMachine).Name);
    }

    public void ChangeState(PerfectState newState)
    {
        if (CurrentState != null)
            CurrentState.Exit();
        PreviousState = CurrentState;
        CurrentState = newState;
        newState.Enter(this);
    }

    public TState GetState<TState>() where TState : PerfectState
    {
        PerfectState result;
        return TryGetState<TState>(out result) ? (TState)result : null;
    }

    private bool TryGetState<TState>(out PerfectState state) where TState : PerfectState
    {
        if (!_isInited)
            Init();
        return _states.TryGetValue(typeof(TState), out state);
    }
}
